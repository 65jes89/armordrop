package com.massivecraft.armordrop.engine;

import com.massivecraft.armordrop.entity.MConf;
import com.massivecraft.massivecore.Engine;
import com.massivecraft.massivecore.util.MUtil;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.HashMap;
import java.util.List;

// Credit for the listener logic goes to Viscar with minor refactoring by 65jes89.

public class EnginePlayerDeath extends Engine
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static EnginePlayerDeath i = new EnginePlayerDeath();
    public static EnginePlayerDeath get() { return i; }

    private static HashMap<String, ItemStack[]> toBeKept = new HashMap<>(); // static abuse?

    // -------------------------------------------- //
    // DEATH EVENT
    // -------------------------------------------- //

    @EventHandler(priority= EventPriority.HIGH)
    public void onDeath(PlayerDeathEvent e)
    {
        if(! MUtil.isPlayer(e.getEntity())) return;

        if(e.getEntity().getWorld().getGameRuleValue("keepInventory").equals("true")) return; // 10/10 api writing, spigot

        boolean keepAllArmor = false;
        if(! MConf.get().shouldArmorDropInWorld(e.getEntity().getWorld().getName())) keepAllArmor = true; // Ensure world whitelisted for armor drops

        Player p = e.getEntity();
        PlayerInventory inv = p.getInventory();
        List<ItemStack> d = e.getDrops();
        ItemStack[] armor = inv.getArmorContents();
        ItemStack[] armorToKeep = new ItemStack[4];

        List<Integer> indicesToKeep = MConf.get().getKeptArmorIndices();
        for(int i = 0; i < 4; i++)
        {
            if(indicesToKeep.contains(i) || ! MConf.get().shouldMaterialDrop(armor[i].getType()) || keepAllArmor)
            {
                armorToKeep[i] = armor[i];
                d.remove(armor[i]);
            }
        }

        toBeKept.put(p.getName(), armorToKeep);
    }

    // -------------------------------------------- //
    // RESPAWN EVENT
    // -------------------------------------------- //

    @EventHandler(priority= EventPriority.HIGH)
    public void onRespawn(PlayerRespawnEvent e)
    {
        Player p = e.getPlayer();
        if(toBeKept.containsKey(p.getName()))
        {
            p.getInventory().setArmorContents(toBeKept.get(p.getName()));
            toBeKept.remove(p.getName());
        }
    }
}