package com.massivecraft.armordrop.cmd;

import com.massivecraft.massivecore.command.MassiveCommand;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import com.massivecraft.armordrop.Perm;
import com.massivecraft.armordrop.entity.MConf;

import java.util.List;

public class CmdArmorDrop extends MassiveCommand
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static CmdArmorDrop i = new CmdArmorDrop();
    public static CmdArmorDrop get() { return i; }

    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //

    private CmdArmorDropConfig cmdArmorDropConfig = new CmdArmorDropConfig();
    private CmdArmorDropVersion cmdArmorDropVersion = new CmdArmorDropVersion();

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdArmorDrop()
    {
        this.addChild(cmdArmorDropConfig);
        this.addChild(cmdArmorDropVersion);

        this.addRequirements(RequirementHasPerm.get(Perm.BASECOMMAND));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesArmorDrop;
    }
}
