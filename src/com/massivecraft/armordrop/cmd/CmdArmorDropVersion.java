package com.massivecraft.armordrop.cmd;

import com.massivecraft.massivecore.command.MassiveCommandVersion;
import com.massivecraft.massivecore.command.requirement.RequirementHasPerm;
import com.massivecraft.armordrop.ArmorDrop;
import com.massivecraft.armordrop.Perm;
import com.massivecraft.armordrop.entity.MConf;

import java.util.List;

public class CmdArmorDropVersion extends MassiveCommandVersion
{
    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public CmdArmorDropVersion()
    {
        super(ArmorDrop.get());

        // Requirements
        this.addRequirements(RequirementHasPerm.get(Perm.VERSION));
    }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public List<String> getAliases()
    {
        return MConf.get().aliasesArmorDropVersion;
    }
}