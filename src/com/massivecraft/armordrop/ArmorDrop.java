package com.massivecraft.armordrop;

import com.massivecraft.armordrop.cmd.CmdArmorDrop;
import com.massivecraft.armordrop.engine.EnginePlayerDeath;
import com.massivecraft.armordrop.entity.MConfColl;
import com.massivecraft.massivecore.MassivePlugin;

public class ArmorDrop extends MassivePlugin
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    private static ArmorDrop i;
    public static ArmorDrop get() { return i; }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    public ArmorDrop() { ArmorDrop.i = this; }

    // -------------------------------------------- //
    // OVERRIDE
    // -------------------------------------------- //

    @Override
    public void onEnableInner()
    {
        this.activate(
            // Engines
            EnginePlayerDeath.class,

            // Entities
            MConfColl.class,

            // Commands
            CmdArmorDrop.class
        );
    }
}
