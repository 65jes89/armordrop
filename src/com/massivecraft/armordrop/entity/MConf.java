package com.massivecraft.armordrop.entity;

import com.massivecraft.massivecore.collections.MassiveList;
import com.massivecraft.massivecore.command.editor.annotation.EditorName;
import com.massivecraft.massivecore.command.editor.annotation.EditorTypeInner;
import com.massivecraft.massivecore.command.type.TypeWorldId;
import com.massivecraft.massivecore.store.Entity;
import com.massivecraft.massivecore.util.IntervalUtil;
import org.bukkit.Material;

import java.util.ArrayList;
import java.util.List;

@EditorName("config")
public class MConf extends Entity<MConf>
{
    // -------------------------------------------- //
    // INSTANCE
    // -------------------------------------------- //

    protected static transient MConf i;
    public static MConf get() { return i; }

    // -------------------------------------------- //
    // COMMAND ALIASES
    // -------------------------------------------- //

    public List<String> aliasesArmorDrop = new MassiveList<>("armordrop");
    public List<String> aliasesArmorDropConfig = new MassiveList<>("config");
    public List<String> aliasesArmorDropVersion = new MassiveList<>("version", "v");

    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //


    // Logic to determine which pieces should drop
    private int numKeptArmorPieces = 2;

    public List<Integer> getKeptArmorIndices()
    {
        List<Integer> ret = new ArrayList<>();
        while(ret.size() < numKeptArmorPieces && ret.size() < 4) // Hard coded value to protect against infinite loop
        {
            int num = IntervalUtil.random(0, 3);

            if(ret.contains(num)) continue;

            ret.add(num);
        }

        return ret;
    }

    private List<Material> materialsEligibleToDropOnDeath = new MassiveList<>(Material.DIAMOND_BOOTS, Material.DIAMOND_LEGGINGS, Material.DIAMOND_CHESTPLATE, Material.DIAMOND_HELMET);

    public boolean shouldMaterialDrop(Material material)
    {
        return materialsEligibleToDropOnDeath.contains(material);
    }

    // Armor drop worlds whitelist
    @EditorTypeInner(TypeWorldId.class)
    private List<String> worldsArmorDropEnabled = new ArrayList<>();

    public boolean shouldArmorDropInWorld(String worldID)
    {
        return worldsArmorDropEnabled.contains(worldID);
    }
}