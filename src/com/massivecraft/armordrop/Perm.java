package com.massivecraft.armordrop;

import com.massivecraft.massivecore.Identified;
import com.massivecraft.massivecore.util.PermissionUtil;

public enum Perm implements Identified
{
    // -------------------------------------------- //
    // ENUM
    // -------------------------------------------- //
    BASECOMMAND,
    CONFIG,
    VERSION
    // END OF LIST
    ;

    // -------------------------------------------- //
    // FIELDS
    // -------------------------------------------- //

    private final String id;

    @Override
    public String getId()
    {
        return this.id;
    }

    // -------------------------------------------- //
    // CONSTRUCT
    // -------------------------------------------- //

    Perm()
    {
        this.id = PermissionUtil.createPermissionId(ArmorDrop.get(), this);
    }
}